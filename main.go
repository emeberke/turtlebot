package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

// Allows APIKeys to be assigned in turtleBot.yaml
type Config struct {
	DiscordToken string `yaml:"discordToken"`
	CatAPI       string `yaml:"catAPI"`
	GoogleKey    string `yaml:"googleKey"`
	GoogleCX     string `yaml:"googleCX"`
	AccessKey    string `yaml:"AccessKey"`
	AWSRegion    string `yaml:"AWSRegion"`
	ServiceName  string `yaml:"ServiceName"`
	XIVAPI       string `yaml:"XIVAPI"`
}

// GoogleResult contains the contents of returned JSON from the Google Custom Search API
type GoogleResult struct {
	Info struct {
		Results string `json:"totalResults"`
	} `json:"searchInformation"`
	Items []Items
}

// Items contains data from GoogleResult
type Items struct {
	Title string
	Link  string
}

// TurtleBot Variables
var (
	StartTime = time.Now()

	Prefix = "~"
	Color  = 0x009688
	Name   = "TurtleBot"
	Icons  = "https://turtleboticons.s3.us-east-2.amazonaws.com"
	Footer = "________________________________________"
	Play   = "Ping Me"
	cfg    Config
)

// This function is called when loading the bot
func init() {
	// Read from config file
	err := cleanenv.ReadConfig("turtleBot.yaml", &cfg)
	if err != nil {
		fmt.Println("Error reading yaml file.")
	}

	flag.StringVar(&cfg.DiscordToken, "t", cfg.DiscordToken, "Bot Token")
	flag.Parse()
}

// Bot startup/closing is handled here
func main() {
	// Read from config file
	err := cleanenv.ReadConfig("turtleBot.yaml", &cfg)
	if err != nil {
		fmt.Println("Error reading yaml file.")
	}

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + cfg.DiscordToken)
	if err != nil {
		fmt.Println("Unable to create discord session!")
		return
	}

	// Setup handlers.
	dg.AddHandler(messageCreate)
	dg.AddHandler(ready)

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("Unable to open connection!")
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

func ready(s *discordgo.Session, event *discordgo.Ready) {
	fmt.Println("Connected to Discord servers!")
	// Set playing status
	s.UpdateGameStatus(0, Play)
}

// This is called on every message received
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	tr := &http.Transport{DisableKeepAlives: true}
	client := &http.Client{Transport: tr}

	// Do not respond to self
	if m.Author.ID == s.State.User.ID {
		return
	}

	// Bot responds to "ping" with "pong"
	if strings.EqualFold(m.Content, "ping") || strings.EqualFold(m.Content, "ping!") {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	// Help embed
	help := &discordgo.MessageEmbed{
		Author: &discordgo.MessageEmbedAuthor{Name: Name + " Commands", IconURL: Icons + "/icon11574.png?X-Amz-Expires=86400&X-Amz-Date=20210219T000354Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=" + cfg.AccessKey +
			"%2F20210219%2F" + cfg.AWSRegion + "%2F" + cfg.ServiceName + "%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=b0303f04ab425ac46e2b707d0711bd77a040e12bf121190c818e9aa272ea223d"},
		Color: Color,
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   Prefix + "help",
				Value:  "Display a list of commands.",
				Inline: true,
			},
			{
				Name:   Prefix + "info",
				Value:  "View " + Name + "'s active configuration and hardware.",
				Inline: true,
			},
			{
				Name:   Prefix + "cat",
				Value:  "Display a random cat picture.",
				Inline: true,
			},
			{
				Name:   Prefix + "rand",
				Value:  "Generate a random number between zero and the number you enter.",
				Inline: true,
			},
			{
				Name:   Prefix + "google",
				Value:  "Search for something in Google.",
				Inline: true,
			},
			{
				Name:   Prefix + "roll",
				Value:  "Roll a dice with the specified number of sides.",
				Inline: true,
			},
		},
		Footer: &discordgo.MessageEmbedFooter{Text: Footer, IconURL: Icons + "/icon11574.png?X-Amz-Expires=86400&X-Amz-Date=20210219T000354Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=" + cfg.AccessKey +
			"%2F20210219%2F" + cfg.AWSRegion + "%2F" + cfg.ServiceName + "%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=b0303f04ab425ac46e2b707d0711bd77a040e12bf121190c818e9aa272ea223d"},
	}
	if strings.HasPrefix(m.Content, Prefix+"help") {
		s.ChannelMessageSendEmbed(m.ChannelID, help)
	}

	// Info embed
	if strings.HasPrefix(m.Content, Prefix+"info") {
		var ostring string
		out, err := exec.Command("/bin/uname", "-mrs").Output()
		if err != nil {
			ostring = runtime.GOOS + " " + runtime.GOARCH
		} else {
			ostring = string(out)
		}
		gate, err := s.Gateway()
		if err != nil {
			gate = "Unknown"
		}
		s.ChannelMessageSendEmbed(m.ChannelID, &discordgo.MessageEmbed{
			Author: &discordgo.MessageEmbedAuthor{Name: Name + " Info", IconURL: Icons + "/icon2.png?X-Amz-Expires=86400&X-Amz-Date=20210219T001507Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=" + cfg.AccessKey +
				"%2F20210219%2F" + cfg.AWSRegion + "%2F" + cfg.ServiceName + "%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=6d85fdc4dcb840ec6af17ba12784ce6b005b20fee429bdade1de4ecdccef9e72"},
			Color: Color,
			Fields: []*discordgo.MessageEmbedField{
				{
					Name:   "Playing Status",
					Value:  Play,
					Inline: true,
				},
				{
					Name:   "Icon Server",
					Value:  Icons,
					Inline: true,
				},
				{
					Name:   "Gateway",
					Value:  gate,
					Inline: true,
				},
				{
					Name:   "Uptime",
					Value:  time.Since(StartTime).String(),
					Inline: true,
				},
			},
			Footer: &discordgo.MessageEmbedFooter{Text: Name + " is currently running " + ostring, IconURL: Icons + "/icon2.png?X-Amz-Expires=86400&X-Amz-Date=20210219T001507Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=" + cfg.AccessKey +
				"%2F20210219%2F" + cfg.AWSRegion + "%2F" + cfg.ServiceName + "%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=6d85fdc4dcb840ec6af17ba12784ce6b005b20fee429bdade1de4ecdccef9e72"},
		})
	}

	// Get google searches with Custom Search API
	if strings.HasPrefix(m.Content, Prefix+"google ") {
		resp, err := client.Get("https://www.googleapis.com/customsearch/v1?prettyPrint=false&key=" + cfg.GoogleKey + "&cx=" + cfg.GoogleCX + "&q=" + m.Content[len(Prefix)+7:])
		if resp != nil {
			defer resp.Body.Close()
		}
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Unable to fetch search results!")
			fmt.Println("[Warning] : Custom Search API Error")
		} else {
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				s.ChannelMessageSend(m.ChannelID, "Unable to download search results!")
				fmt.Println("[Warning] : IO Error")
			} else {
				res := &GoogleResult{}
				err = json.Unmarshal(body, res)
				if err != nil || len(res.Items) == 0 {
					s.ChannelMessageSend(m.ChannelID, "Unable to find search results!")
					fmt.Println("[Warning] : Parsing Error ")
				} else {
					results := &discordgo.MessageEmbed{
						Author: &discordgo.MessageEmbedAuthor{Name: "Google Results for " + m.Content[len(Prefix)+7:] + " (" + res.Info.Results + " Total Results)", IconURL: Icons + "/icon4.png?X-Amz-Expires=86400&X-Amz-Date=20210219T001453Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=" + cfg.AccessKey +
							"%2F20210219%2F" + cfg.AWSRegion + "%2F" + cfg.ServiceName + "%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=cb4dd87ffbbace8061472d51916481eb357dc8384e622f81d6b77ca883a68861"},
						Color: Color,
						Footer: &discordgo.MessageEmbedFooter{Text: "Search results provided by Google Custom Search.", IconURL: Icons + "/icon4.png?X-Amz-Expires=86400&X-Amz-Date=20210219T001453Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=" + cfg.AccessKey +
							"%2F20210219%2F" + cfg.AWSRegion + "%2F" + cfg.ServiceName + "%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=cb4dd87ffbbace8061472d51916481eb357dc8384e622f81d6b77ca883a68861"},
					}
					if len(res.Items) > 5 {
						for i := 0; i <= 5; i++ {
							toAdd := &discordgo.MessageEmbedField{
								Name:   res.Items[i].Title,
								Value:  res.Items[i].Link,
								Inline: true,
							}
							results.Fields = append(results.Fields, toAdd)
						}
					} else {
						for i := 0; i < len(res.Items); i++ {
							toAdd := &discordgo.MessageEmbedField{
								Name:   res.Items[i].Title,
								Value:  res.Items[i].Link,
								Inline: true,
							}
							results.Fields = append(results.Fields, toAdd)
						}
					}
					s.ChannelMessageSendEmbed(m.ChannelID, results)
					fmt.Println("[Info] : Search results for '" + m.Content[len(Prefix)+7:] + "' successfully to " + m.Author.Username + "(" + m.Author.ID + ") in " + m.ChannelID)
				}
			}
		}
	}

	// Generate random numbers!
	if strings.HasPrefix(m.Content, Prefix+"rand ") {
		input, err := strconv.Atoi(m.Content[len(Prefix)+5:])
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Input invalid!")
			fmt.Println("[Warning] : Parsing Error")
		} else {
			s.ChannelMessageSend(m.ChannelID, strconv.Itoa(rand.Intn(input)))
		}
	}

	// Roll a die
	if strings.HasPrefix(m.Content, Prefix+"roll ") {
		input := strings.Split(m.Content[len(Prefix)+5:], "d")
		numDie, err := strconv.Atoi(input[0])
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Input invalid!")
			fmt.Println("[Warning] : Parsing Error")
		}

		faces, err := strconv.Atoi(input[1])
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Input invalid!")
			fmt.Println("[Warning] : Parsing Error")
		}
		rolledValue := ""
		sum := 0
		if numDie > 1 {
			for i := 0; i < numDie; i++ {
				rolledNum := rand.Intn(faces) + 1
				sum += rolledNum
				rolledValue += " " + strconv.Itoa(rolledNum)
			}
			rolledValue += " (" + strconv.Itoa(sum) + ")"
			s.ChannelMessageSend(m.ChannelID, rolledValue)
		} else {
			s.ChannelMessageSend(m.ChannelID, strconv.Itoa(rand.Intn(faces)+1))
		}
	}

	// Get pictures with the Cat API
	if strings.HasPrefix(m.Content, Prefix+"cat") {
		resp, err := client.Get("http://thecatAPI.com/api/images/get?api_key=" + cfg.CatAPI + "&format=src")
		if resp != nil {
			defer resp.Body.Close()
		}
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Unable to fetch cat!")
			fmt.Println("[Warning] : Cat API Error")
		} else {
			s.ChannelMessageSendEmbed(m.ChannelID, &discordgo.MessageEmbed{
				Author: &discordgo.MessageEmbedAuthor{Name: "Cat Picture", IconURL: Icons + "/icon6.png?X-Amz-Expires=86400&X-Amz-Date=20210219T002001Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=" + cfg.AccessKey +
					"%2F20210219%2F" + cfg.AWSRegion + "%2F" + cfg.ServiceName + "%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=4fba289bfa62495158babe0fd2393efcdfdf0898fc7874f47b835f270719e117"},
				Color: Color,
				Image: &discordgo.MessageEmbedImage{
					URL: resp.Request.URL.String(),
				},
				Footer: &discordgo.MessageEmbedFooter{Text: "Cat pictures provided by TheCatAPI", IconURL: Icons + "/icon6.png?X-Amz-Expires=86400&X-Amz-Date=20210219T002001Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=" + cfg.AccessKey +
					"%2F20210219%2F" + cfg.AWSRegion + "%2F" + cfg.ServiceName + "%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=4fba289bfa62495158babe0fd2393efcdfdf0898fc7874f47b835f270719e117"},
			})
			fmt.Println("[Info] : Cat sent successfully to " + m.Author.Username + "(" + m.Author.ID + ") in " + m.ChannelID)
		}
	}
}
